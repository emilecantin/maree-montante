const fs = require('fs');
const cheerio = require('cheerio');
const moment = require('moment');

function parseHtml(html) {
  const data = [];

  const $ = cheerio.load(html);

  const tables = $('table').toArray();
  let month = 1;
  for(let table of tables) {
    // console.log(table);
    // console.log($('caption', table).text());
    const rows = $('tbody tr', table).toArray();
    for(let row of rows) {
      const day = $('td:nth-child(1)', row).text();
      const hour = $('td:nth-child(2)', row).text();
      const height = $('td:nth-child(3)', row).text();
      const date = moment(`2019-${month}-${day} ${hour} -0500`, 'Y-M-D HH:mm ZZ');
      // console.log(date.format('Y-MM-DD HH:mm'), height);
      data.push({
        date,
        height
      });
      // console.log(day, hour, height);
    }
    month++;
  }
  return data;
}

const pphtml = fs.readFileSync(__dirname + '/2019/pp.html', 'utf8');
const qchtml = fs.readFileSync(__dirname + '/2019/qc.html', 'utf8');
console.log(JSON.stringify([{
  name: 'Pointe-au-Père',
  data: parseHtml(pphtml),
}, {
  name: 'Québec',
  data: parseHtml(qchtml),
}]));
