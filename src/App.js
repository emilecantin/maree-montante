import React, {useState} from 'react';
import moment from 'moment';
import 'moment/locale/fr';
import logo from './logo.svg';
import './App.css';

import stations from './data/2019/data.json';

moment('fr');

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {stations.map(station => (
          <Station station={station} />
        ))}
      </header>
    </div>
  );
}

function Station(props) {
  const {station} = props;
  const date = moment();
  let index = 0;
  while (date > moment(station.data[index].date)) {
    index++;
  }
  const prev = station.data[index-1];
  const next = station.data[index];
  let prevLvl = 'BM';
  let nextLvl = 'PM';
  if(prev.height > next.height) {
    prevLvl = 'PM';
    nextLvl = 'BM';
  }
  const prevMoment = moment(prev.date);
  const nextMoment = moment(next.date);
  return (
    <div>
      <h2>{station.name}</h2>
      <p>
        {prevLvl} {prevMoment.fromNow()}
        <br />
        <span className="small">
          ({prev.height}m @ {prevMoment.format('HH:mm')})
        </span>
      </p>
      <p>
        {nextLvl} {nextMoment.fromNow()}
        <br />
        <span className="small">
          ({next.height}m @ {nextMoment.format('HH:mm')})
        </span>
      </p>
    </div>
  );
}

export default App;
